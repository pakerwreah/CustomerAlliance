import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

const http = axios.create({
  baseURL: process.env.VUE_APP_WS_ROOT,
  timeout: 30000
})

Vue.use(VueAxios, http)
