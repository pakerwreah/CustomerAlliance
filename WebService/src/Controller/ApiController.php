<?php

namespace App\Controller;

use App\Entity\Review;
use App\Repository\HotelRepository;
use App\Repository\ReviewRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/average", name="average", methods={"GET"})
     */
    public function getAverage(Request $request, HotelRepository $hotelRepository, ReviewRepository $reviewRepository)
    {
        $uuid = $request->get('uuid');

        $hotel = $hotelRepository->findOneBy(['uuid' => $uuid]);

        if ($hotel === null) {
            throw new NotFoundHttpException('Hotel not found.');
        }

        $query = $reviewRepository->createQueryBuilder('r');
        $exp = $query->expr();

        $score = $query
            ->select($exp->avg('r.score'))
            ->where($exp->eq('r.hotel_id', ':hotel_id'))
            ->setParameter('hotel_id', $hotel->getId())
            ->getQuery()
            ->getSingleScalarResult();

        return $this->json(compact('score'));
    }

    /**
     * @Route("/api/reviews", name="review_list", methods={"GET"})
     */
    public function getReviews(Request $request, HotelRepository $hotelRepository)
    {
        $uuid = $request->get('uuid');

        $hotel = $hotelRepository->findOneBy(['uuid' => $uuid]);

        if ($hotel === null) {
            throw new NotFoundHttpException('Hotel not found.');
        }

        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        $reviews = $serializer->serialize($hotel->getReviews(), 'json', ['ignored_attributes' => ['hotel']]);

        return JsonResponse::fromJsonString($reviews);
    }

    /**
     * @Route("/api/reviews", name="review_add", methods={"POST"})
     */
    public function setReview(Request $request, HotelRepository $hotelRepository)
    {
        $input = json_decode($request->getContent(), true) ?? [];

        $validator = Validation::createValidator();

        $constraints = new Assert\Collection([
            'uuid' => new Assert\Uuid(),
            'score' => new Assert\Required([
                new Assert\Type('int'),
                new Assert\Range(['min' => 1, 'max' => 5])
            ]),
            'comment' => new Assert\Optional([
                new Assert\Type('string'),
                new Assert\Length(['max' => 255])
            ])
        ]);

        $violations = $validator->validate($input, $constraints);

        if(count($violations) > 0) {
            $accessor = PropertyAccess::createPropertyAccessor();

            $errorMessages = [];

            foreach ($violations as $v) {
                $accessor->setValue($errorMessages, $v->getPropertyPath(), $v->getMessage());
            }

            return $this->json($errorMessages, Response::HTTP_BAD_REQUEST);
        }

        $uuid = $input['uuid'];
        $score = $input['score'];
        $comment = $input['comment'] ?? '';

        $hotel = $hotelRepository->findOneBy(compact('uuid'));

        if ($hotel === null) {
            throw new NotFoundHttpException('Hotel not found.');
        }

        $manager = $this->getDoctrine()->getManager();

        $review = new Review();
        $review->setHotel($hotel);
        $review->setScore($score);
        $review->setComment($comment);
        $manager->persist($review);
        $manager->flush();

        return $this->json(['message' => 'Review added successfully!']);
    }
}
