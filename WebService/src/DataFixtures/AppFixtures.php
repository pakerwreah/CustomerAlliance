<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Review;
use App\Repository\HotelRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->loadHotels($manager);
        $manager->flush();
        $this->loadReviews($manager);
        $manager->flush();
    }

    public function loadHotels($manager)
    {
        $hotel = new Hotel();
        $hotel->setId(1);
        $hotel->setName('Hotel Alexanderplatz');
        $hotel->setAddress('Alexanderplatz 1, 10409, Berlin');

        $manager->persist($hotel);

        $hotel = new Hotel();
        $hotel->setId(2);
        $hotel->setName('Hotel Alexanderplatz');
        $hotel->setAddress('Alexanderplatz 1, 10409, Berlin');

        $manager->persist($hotel);

        $hotel = new Hotel();
        $hotel->setId(3);
        $hotel->setName('Hotel Alexanderplatz');
        $hotel->setAddress('Alexanderplatz 1, 10409, Berlin');

        $manager->persist($hotel);

        $hotel = new Hotel();
        $hotel->setId(4);
        $hotel->setName('Hotel Alexanderplatz');
        $hotel->setAddress('Alexanderplatz 1, 10409, Berlin');

        $manager->persist($hotel);

        $hotel = new Hotel();
        $hotel->setId(5);
        // set uuid to test the frontend app
        $hotel->setUuid('50c64598-be1e-4ed7-b047-73c0a76a3bce');
        $hotel->setName('Hotel Alexanderplatz');
        $hotel->setAddress('Alexanderplatz 1, 10409, Berlin');

        $manager->persist($hotel);
    }

    public function loadReviews(ObjectManager $manager)
    {
        /** @var HotelRepository $hotelRepository */
        $hotelRepository = $manager->getRepository(Hotel::class);

        // hotel 1
        $hotel = $hotelRepository->find(1);

        $review = new Review();
        $review->setHotel($hotel);
        $review->setComment('Very nice stay');
        $review->setScore(10);
        $manager->persist($review);

        $review = new Review();
        $review->setHotel($hotel);
        $review->setComment('Average');
        $review->setScore(5);
        $manager->persist($review);

        $review = new Review();
        $review->setHotel($hotel);
        $review->setComment('Very nice stay, I enjoyed it a lot.');
        $review->setScore(9);
        $manager->persist($review);

        $review = new Review();
        $review->setHotel($hotel);
        $review->setComment('Worst experience ever.');
        $review->setScore(1);
        $manager->persist($review);

        // hotel 2
        $hotel = $hotelRepository->find(2);

        $review = new Review();
        $review->setHotel($hotel);
        $review->setComment('The receptionist was not smiling.');
        $review->setScore(5);
        $manager->persist($review);

        $review = new Review();
        $review->setHotel($hotel);
        $review->setComment('Very nice stay, the reception was really fast.');
        $review->setScore(10);
        $manager->persist($review);
    }
}
