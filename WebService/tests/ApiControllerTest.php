<?php

namespace App\Tests\Util;

use App\Entity\Hotel;
use App\Entity\Review;
use App\Repository\HotelRepository;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use function json_encode;
use function random_int;

class ApiControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private ObjectManager $manager;

    protected function setUp() {
        parent::setUp();

        $this->client = static::createClient();

        $this->manager = $this->client->getContainer()->get('doctrine')->getManager();
    }

    public function testGetAverage()
    {
        $client = $this->client;

        /** @var HotelRepository $hotelRepository */
        $hotelRepository = $this->manager->getRepository(Hotel::class);

        // hotel 1

        $hotel = $hotelRepository->find(1);

        $client->request('GET', '/api/average?uuid='.$hotel->getUuid());

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"score":"6.25"}', $response->getContent());

        // hotel 2

        $hotel = $hotelRepository->find(2);

        $client->request('GET', '/api/average?uuid='.$hotel->getUuid());

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"score":"7.5"}', $response->getContent());
    }

    public function testGetReviews()
    {
        $client = $this->client;

        /** @var HotelRepository $hotelRepository */
        $hotelRepository = $this->manager->getRepository(Hotel::class);

        // hotel 1

        $hotel = $hotelRepository->find(1);

        $client->request('GET', '/api/reviews?uuid='.$hotel->getUuid());

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('[{"id":1,"hotel_id":1,"score":10,"comment":"Very nice stay"},{"id":2,"hotel_id":1,"score":5,"comment":"Average"},{"id":3,"hotel_id":1,"score":9,"comment":"Very nice stay, I enjoyed it a lot."},{"id":4,"hotel_id":1,"score":1,"comment":"Worst experience ever."}]', $response->getContent());

        // hotel 2

        $hotel = $hotelRepository->find(2);

        $client->request('GET', '/api/reviews?uuid='.$hotel->getUuid());

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('[{"id":5,"hotel_id":2,"score":5,"comment":"The receptionist was not smiling."},{"id":6,"hotel_id":2,"score":10,"comment":"Very nice stay, the reception was really fast."}]', $response->getContent());
    }

    public function testSetReviews() {
        $client = $this->client;

        /** @var HotelRepository $hotelRepository */
        $hotelRepository = $this->manager->getRepository(Hotel::class);

        $hotel = $hotelRepository->find(3);

        $count_before = $hotel->getReviews()->count();

        $review_uuid = Uuid::uuid4()->toString();

        $score = random_int(1, 5);

        $client->request('POST', '/api/reviews',[],[],[],json_encode([
            'uuid' => $hotel->getUuid(),
            'score' => $score,
            'comment' => $review_uuid
        ]));

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{"message":"Review added successfully!"}', $response->getContent());

        $this->manager->refresh($hotel);

        $reviews = $hotel->getReviews();

        /** @var Review $last_review */
        $last_review = $reviews->last();

        $this->assertEquals($count_before + 1, $reviews->count());
        $this->assertEquals($score, $last_review->getScore());
        $this->assertEquals($review_uuid, $last_review->getComment());
    }

    public function testSetReviewsParameters() {
        $client = $this->client;

        /** @var HotelRepository $hotelRepository */
        $hotelRepository = $this->manager->getRepository(Hotel::class);

        $successMessage = '{"message":"Review added successfully!"}';

        $hotel = $hotelRepository->find(3);

        // Missing parameters

        $client->request('POST', '/api/reviews',[],[],[],json_encode([
            // nothing
        ]));

        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('{"uuid":"This field is missing.","score":"This field is missing."}', $response->getContent());

        $client->request('POST', '/api/reviews',[],[],[],json_encode([
            'uuid' => $hotel->getUuid()
            // missing score
        ]));

        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('{"score":"This field is missing."}', $response->getContent());

        // UUID not valid

        $client->request('POST', '/api/reviews',[],[],[],json_encode([
            'uuid' => 'invalid',
            'score' => 3
        ]));

        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('{"uuid":"This is not a valid UUID."}', $response->getContent());

        // Score not valid

        $client->request('POST', '/api/reviews',[],[],[],json_encode([
            'uuid' => '6e43203c-aa91-4cf4-9a0a-9aac3acc254b',
            'score' => 10
        ]));

        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('{"score":"This value should be between 1 and 5."}', $response->getContent());

        // Hotel not found

        $client->request('POST', '/api/reviews',[],[],[],json_encode([
            'uuid' => '6e43203c-aa91-4cf4-9a0a-000000000000',
            'score' => 1
        ]));

        $response = $client->getResponse();

        $this->assertEquals(404, $response->getStatusCode());

        // Success without comment

        $client->request('POST', '/api/reviews',[],[],[],json_encode([
            'uuid' => $hotel->getUuid(),
            'score' => 1
        ]));

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($successMessage, $response->getContent());
    }
}
